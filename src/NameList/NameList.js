import React, {useState} from 'react';
import './NameList.css'

const NameList = () => {
  const [names, setNames] = useState([]);

  function updateName(i, propertyName, value) {
    const updatedNames = [...names];
    updatedNames[i][propertyName] = value;
    setNames(updatedNames);
  }

  function addName() {
    setNames([...names, {firstName: "", lastName: ""}]);
  }

  return <div className="nameList">
    <div className="nameInputs">
      {names.map((name, i) =>
        <NameInput
          key={i}
          inputIndex={i}
          handleNameUpdate={(propertyName, value) => updateName(i, propertyName, value)}/>
      )}
      <button onClick={() => addName()}>Add name</button>
    </div>
    <div className="names">
      <ul>
        {names.map((name, i) => <li key={i}>{name.firstName} {name.lastName}</li>)}
      </ul>
    </div>
  </div>;
}

const NameInput = props =>
  <fieldset className="nameInput">
    <legend>Name {props.inputIndex + 1}</legend>

    <label htmlFor={`firstName-${props.inputIndex}`}>First name:</label>
    <input
      id={`firstName-${props.inputIndex}`}
      onChange={e => props.handleNameUpdate("firstName", e.target.value)}/>

    <label htmlFor={`lastName-${props.inputIndex}`}>Last name:</label>
    <input
      id={`lastName-${props.inputIndex}`}
      onChange={e => props.handleNameUpdate("lastName", e.target.value)}/>
  </fieldset>

export default NameList;
