import React from 'react';
import './App.css';
import NameList from './NameList/NameList';

function App() {
  return (
    <div className="App">
      <NameList/>
    </div>
  );
}

export default App;
